terraform {
  backend "http" {}
}

module "my_module_name" {
  source = "gitlab.com/mattkasa/gitlab-file/local"
  version = "0.0.3"

  text = "Hello World"
  filename = "hello"
}

output "filesize_in_bytes" {
  value = module.my_module_name.bytes
}